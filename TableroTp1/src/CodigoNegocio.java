import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JTextField;

public class CodigoNegocio {
	
	public Integer[]list;
	
	public CodigoNegocio(){
		
		//Generar los numeros de la grilla
		list = new Integer[16];
		int i =0;
		while(i<16) {
			
			list[i] = (int) Math.floor(Math.random()*10);
			i++;
		}
		
	}
	//Verificar estandar
	//se opto por usar Elemento para que sea mas entendible a alguien ajeno al codigo. Se pudo usar 
	//una variable mas corta para evitar escribir tanto.
	public boolean verificar(int Elemento1, int Elemento2, int Elemento3, int Elemento4, int result) {
		if(Elemento1 + Elemento2 + Elemento3 + Elemento4 == result)
			return true;
	
		return false;
	}
	
	//Setear los resultados en las label
	public int resultados(int a) {

		if (a==0) //fila0
			return list[0]+list[1]+list[2]+list[3];
		if(a==1) //fila1
			return list[4]+list[5]+list[6]+list[7];
		if(a==2) //fila2
			return list[8]+list[9]+list[10]+list[11];
		if(a==3) //fila 3
			return list[12]+list[13]+list[14]+list[15];
		if(a==4)//columna0
			return list[0]+list[4]+list[8]+list[12];
		if(a==5) //columna1
			return list[1]+list[5]+list[9]+list[13];
		if(a==6)//columna2
			return list[2]+list[6]+list[10]+list[14];
		if(a==7) //columna3
			return list[3]+list[7]+list[11]+list[15];		
		
		return 0;
		
	}
	//Verifica las filas
	public boolean verificarFilas() {
		if((verificar(Integer.parseInt(Tabla.F0C0.getText()),Integer.parseInt(Tabla.F0C1.getText()), 
		Integer.parseInt(Tabla.F0C2.getText()), Integer.parseInt(Tabla.F0C3.getText()), Integer.parseInt(Tabla.ResultadoFila0.getText())) 
		
		&&
		
		(verificar(Integer.parseInt(Tabla.F1C0.getText()),Integer.parseInt(Tabla.F1C1.getText()), 
				Integer.parseInt(Tabla.F1C2.getText()), Integer.parseInt(Tabla.F1C3.getText()), Integer.parseInt(Tabla.ResultadoFila1.getText()))) 
				
		&&
		
		(verificar(Integer.parseInt(Tabla.F2C0.getText()),Integer.parseInt(Tabla.F2C1.getText()), 
				Integer.parseInt(Tabla.F2C2.getText()), Integer.parseInt(Tabla.F2C3.getText()), Integer.parseInt(Tabla.ResultadoFila2.getText()))) 
		&&
		
		(verificar(Integer.parseInt(Tabla.F3C0.getText()),Integer.parseInt(Tabla.F3C1.getText()), 
				Integer.parseInt(Tabla.F3C2.getText()), Integer.parseInt(Tabla.F3C3.getText()), Integer.parseInt(Tabla.ResultadoFila3.getText())))))
			return true;
		
		return false;
		
	}
	//verifica las columnas
	public boolean verificarColumnas() {
		if((verificar(Integer.parseInt(Tabla.F0C0.getText()),Integer.parseInt(Tabla.F1C0.getText()), 
				Integer.parseInt(Tabla.F2C0.getText()), Integer.parseInt(Tabla.F3C0.getText()), Integer.parseInt(Tabla.ResultadoCol0.getText())))
	
		&&
		
		(verificar(Integer.parseInt(Tabla.F0C1.getText()),Integer.parseInt(Tabla.F1C1.getText()), 
				Integer.parseInt(Tabla.F2C1.getText()), Integer.parseInt(Tabla.F3C1.getText()), Integer.parseInt(Tabla.ResultadoCol1.getText())))
	
		&&
		
		(verificar(Integer.parseInt(Tabla.F0C2.getText()),Integer.parseInt(Tabla.F1C2.getText()), 
				Integer.parseInt(Tabla.F2C2.getText()), Integer.parseInt(Tabla.F3C2.getText()), Integer.parseInt(Tabla.ResultadoCol2.getText())))
		&&
		
		(verificar(Integer.parseInt(Tabla.F0C3.getText()),Integer.parseInt(Tabla.F1C3.getText()), 
				Integer.parseInt(Tabla.F2C3.getText()), Integer.parseInt(Tabla.F3C3.getText()), Integer.parseInt(Tabla.ResultadoCol3.getText()))))
			return true;
		
		
		return false;
		
	}
	
	public boolean completado() {
		boolean res = true;
		if(Tabla.F0C0.getText().isEmpty() || Tabla.F0C1.getText().isEmpty() || Tabla.F0C2.getText().isEmpty() || Tabla.F0C3.getText().isEmpty()
				|| Tabla.F1C1.getText().isEmpty() || Tabla.F1C1.getText().isEmpty() || Tabla.F1C2.getText().isEmpty() || Tabla.F1C3.getText().isEmpty()
				|| Tabla.F2C1.getText().isEmpty() || Tabla.F2C1.getText().isEmpty() || Tabla.F2C2.getText().isEmpty() || Tabla.F2C3.getText().isEmpty() 
				|| Tabla.F3C1.getText().isEmpty() || Tabla.F3C1.getText().isEmpty() || Tabla.F3C2.getText().isEmpty() || Tabla.F3C3.getText().isEmpty()
				
				)	
			res = false;		
		return res;
	}
	
	public String mostrarResultados() {
		
		return ""+ list[0]+ list[1]+ list[2]+ list[3] + " "+ 
				list[4]+ list[5]+ list[6]+ list[7] +  " "+
				list[8]+ list[9]+ list[10]+ list[11]+  " "+
				list[12]+ list[13]+ list[14]+ list[15];
		
	}
	
	
	
	public void LogicaDeJuego() {
		if(Tabla.negocio.completado()) {
			if ( Tabla.negocio.verificarFilas() && Tabla.negocio.verificarColumnas()) {
				VentanaCorrecto correcto = new VentanaCorrecto();
				correcto.frame.setVisible(true);	
			}
			else {
				VentanaIncorrecto incorrecto = new VentanaIncorrecto();
				incorrecto.frame.setVisible(true);
			}
		}
		else {
			VentanaDeAviso ventAviso = new VentanaDeAviso();
			ventAviso.frame.setVisible(true);
		}
	}
	public static void reintentar() {
		Tabla.F0C0.setText("");
		Tabla.F0C1.setText("");
		Tabla.F0C2.setText("");
		Tabla.F0C3.setText("");
		Tabla.F1C0.setText("");
		Tabla.F1C1.setText("");
		Tabla.F1C2.setText("");
		Tabla.F1C3.setText("");
		Tabla.F2C0.setText("");
		Tabla.F2C1.setText("");
		Tabla.F2C2.setText("");
		Tabla.F2C3.setText("");
		Tabla.F3C0.setText("");
		Tabla.F3C1.setText("");
		Tabla.F3C2.setText("");
		Tabla.F3C3.setText("");
	}
	
	public void verificarNumero(JTextField FilaColumna){
		FilaColumna.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				char validar = e.getKeyChar();
				if (!Character.isDigit(validar)) {
					VentanaDeAviso ventAviso = new VentanaDeAviso();
					ventAviso.frame.setVisible(true);
				}
			}
		});
	}
	
	public void verificarIngresoDeNumero() {
		verificarNumero(Tabla.F0C0);
		verificarNumero(Tabla.F0C1);
		verificarNumero(Tabla.F0C2);
		verificarNumero(Tabla.F0C3);
		verificarNumero(Tabla.F1C0);
		verificarNumero(Tabla.F1C1);
		verificarNumero(Tabla.F1C2);
		verificarNumero(Tabla.F1C3);
		verificarNumero(Tabla.F2C0);
		verificarNumero(Tabla.F2C1);
		verificarNumero(Tabla.F2C2);
		verificarNumero(Tabla.F2C3);
		verificarNumero(Tabla.F3C0);
		verificarNumero(Tabla.F3C1);
		verificarNumero(Tabla.F3C2);
		verificarNumero(Tabla.F3C3);
	}
	
/**	
	public static void juegoNuevo() {
		Tabla.negocio = new CodigoNegocio();
	}*/
}
