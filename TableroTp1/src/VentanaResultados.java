import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class VentanaResultados {

	JFrame frame;
	private JTextField VentanaNueva;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VentanaResultados window = new VentanaResultados();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public VentanaResultados() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 335, 190);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JButton btnNewButton = new JButton("OK");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.setVisible(false);
			}
		});
		btnNewButton.setBounds(109, 110, 85, 21);
		frame.getContentPane().add(btnNewButton);
		
		VentanaNueva = new JTextField();
		VentanaNueva.setToolTipText("");
		VentanaNueva.setFont(new Font("Tahoma", Font.PLAIN, 19));
		VentanaNueva.setEditable(false);
		VentanaNueva.setBounds(37, 10, 239, 80);
		frame.getContentPane().add(VentanaNueva);
		VentanaNueva.setColumns(10);
	}
	
	public void mostrar(CodigoNegocio neg) {
		VentanaNueva.setText(neg.mostrarResultados());
	}
}
