import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Toolkit;
import java.awt.Font;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JLabel;


public class Tabla {

	private JFrame frmTablero;
	public static JTextField F0C0;
	public static JTextField F1C0;
	public static JTextField F2C0;
	public static JTextField F3C0;
	public static JTextField F0C1;
	public static JTextField F1C1;
	public static JTextField F2C1;
	public static JTextField F3C1;
	public static JTextField F0C2;
	public static JTextField F1C2;
	public static JTextField F2C2;
	public static JTextField F3C2;
	public static JTextField F0C3;
	public static JTextField F1C3;
	public static JTextField F2C3;
	public static JTextField F3C3;

	public static JTextField ResultadoFila0;
	public static JTextField ResultadoFila1;
	public static JTextField ResultadoFila2;
	public static JTextField ResultadoFila3;
	public static JTextField ResultadoCol0;
	public static JTextField ResultadoCol1;
	public static JTextField ResultadoCol2;
	public static JTextField ResultadoCol3;

	public static CodigoNegocio negocio = new CodigoNegocio();
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Tabla window = new Tabla();
					window.frmTablero.setVisible(true);
					System.out.println(negocio.mostrarResultados());
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Tabla() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmTablero = new JFrame();
		frmTablero.setIconImage(Toolkit.getDefaultToolkit().getImage(Tabla.class.getResource("/Imagenes/correcto.png")));
		frmTablero.setTitle("TABLERO");
		frmTablero.setBounds(100, 100, 507, 323);
		frmTablero.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmTablero.getContentPane().setLayout(null);
		//frmTablero.setUndecorated(true);
		
		JButton btnFinalizar = new JButton("FINALIZAR");
		btnFinalizar.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnFinalizar.addActionListener(new ActionListener() {	
		public void actionPerformed(ActionEvent e) {
				negocio.LogicaDeJuego();
			}	
		});
		
		btnFinalizar.setBounds(313, 218, 154, 47);
		frmTablero.getContentPane().add(btnFinalizar);
		
		
		F0C0 = new JTextField();
		F0C0.setFont(new Font("Tahoma", Font.PLAIN, 20));
		F0C0.setBounds(40, 28, 42, 36);
		frmTablero.getContentPane().add(F0C0);
		F0C0.setColumns(10);
		
		F1C0 = new JTextField();
		F1C0.setFont(new Font("Tahoma", Font.PLAIN, 20));
		F1C0.setColumns(10);
		F1C0.setBounds(40, 74, 42, 36);
		frmTablero.getContentPane().add(F1C0);
		
		F2C0 = new JTextField();
		F2C0.setFont(new Font("Tahoma", Font.PLAIN, 20));
		F2C0.setColumns(10);
		F2C0.setBounds(40, 120, 42, 36);
		frmTablero.getContentPane().add(F2C0);
		
		F3C0 = new JTextField();
		F3C0.setFont(new Font("Tahoma", Font.PLAIN, 20));
		F3C0.setColumns(10);
		F3C0.setBounds(40, 166, 42, 36);
		frmTablero.getContentPane().add(F3C0);
		
		F0C1 = new JTextField();
		F0C1.setFont(new Font("Tahoma", Font.PLAIN, 20));
		F0C1.setColumns(10);
		F0C1.setBounds(99, 28, 42, 36);
		frmTablero.getContentPane().add(F0C1);
		
		F1C1 = new JTextField();
		F1C1.setFont(new Font("Tahoma", Font.PLAIN, 20));
		F1C1.setColumns(10);
		F1C1.setBounds(99, 74, 42, 36);
		frmTablero.getContentPane().add(F1C1);
		
		F2C1 = new JTextField();
		F2C1.setFont(new Font("Tahoma", Font.PLAIN, 20));
		F2C1.setColumns(10);
		F2C1.setBounds(99, 120, 42, 36);
		frmTablero.getContentPane().add(F2C1);
		
		F3C1 = new JTextField();
		F3C1.setFont(new Font("Tahoma", Font.PLAIN, 20));
		F3C1.setColumns(10);
		F3C1.setBounds(99, 166, 42, 36);
		frmTablero.getContentPane().add(F3C1);
		
		F0C2 = new JTextField();
		F0C2.setFont(new Font("Tahoma", Font.PLAIN, 20));
		F0C2.setColumns(10);
		F0C2.setBounds(161, 28, 42, 36);
		frmTablero.getContentPane().add(F0C2);
		
		F1C2 = new JTextField();
		F1C2.setFont(new Font("Tahoma", Font.PLAIN, 20));
		F1C2.setColumns(10);
		F1C2.setBounds(161, 74, 42, 36);
		frmTablero.getContentPane().add(F1C2);
		
		F2C2 = new JTextField();
		F2C2.setFont(new Font("Tahoma", Font.PLAIN, 20));
		F2C2.setColumns(10);
		F2C2.setBounds(161, 120, 42, 36);
		frmTablero.getContentPane().add(F2C2);
		
		F3C2 = new JTextField();
		F3C2.setFont(new Font("Tahoma", Font.PLAIN, 20));
		F3C2.setColumns(10);
		F3C2.setBounds(161, 166, 42, 36);
		frmTablero.getContentPane().add(F3C2);
		
		F0C3 = new JTextField();
		F0C3.setFont(new Font("Tahoma", Font.PLAIN, 20));
		F0C3.setColumns(10);
		F0C3.setBounds(222, 28, 42, 36);
		frmTablero.getContentPane().add(F0C3);
		
		F1C3 = new JTextField();
		F1C3.setFont(new Font("Tahoma", Font.PLAIN, 20));
		F1C3.setColumns(10);
		F1C3.setBounds(222, 74, 42, 36);
		frmTablero.getContentPane().add(F1C3);
		
		F2C3 = new JTextField();
		F2C3.setFont(new Font("Tahoma", Font.PLAIN, 20));
		F2C3.setColumns(10);
		F2C3.setBounds(222, 120, 42, 36);
		frmTablero.getContentPane().add(F2C3);
		
		F3C3 = new JTextField();
		F3C3.setFont(new Font("Tahoma", Font.PLAIN, 20));
		F3C3.setColumns(10);
		F3C3.setBounds(222, 166, 42, 36);
		frmTablero.getContentPane().add(F3C3);
		
		
		ResultadoFila0 = new JTextField();
		ResultadoFila0.setFont(new Font("Tahoma", Font.PLAIN, 19));
		ResultadoFila0.setEditable(false);
		ResultadoFila0.setBounds(288, 34, 33, 31);
		frmTablero.getContentPane().add(ResultadoFila0);
		ResultadoFila0.setColumns(10);
		ResultadoFila0.setText(""+negocio.resultados(0));
		
		ResultadoFila1 = new JTextField();
		ResultadoFila1.setText("0");
		ResultadoFila1.setFont(new Font("Tahoma", Font.PLAIN, 19));
		ResultadoFila1.setEditable(false);
		ResultadoFila1.setColumns(10);
		ResultadoFila1.setBounds(288, 74, 33, 31);
		frmTablero.getContentPane().add(ResultadoFila1);
		ResultadoFila1.setText(""+negocio.resultados(1));
		
		ResultadoFila2 = new JTextField();
		ResultadoFila2.setText("0");
		ResultadoFila2.setFont(new Font("Tahoma", Font.PLAIN, 19));
		ResultadoFila2.setEditable(false);
		ResultadoFila2.setColumns(10);
		ResultadoFila2.setBounds(287, 120, 33, 31);
		frmTablero.getContentPane().add(ResultadoFila2);
		ResultadoFila2.setText(""+negocio.resultados(2));
		
		
		ResultadoFila3 = new JTextField();
		ResultadoFila3.setText("0");
		ResultadoFila3.setFont(new Font("Tahoma", Font.PLAIN, 19));
		ResultadoFila3.setEditable(false);
		ResultadoFila3.setColumns(10);
		ResultadoFila3.setBounds(288, 166, 33, 31);
		frmTablero.getContentPane().add(ResultadoFila3);
		ResultadoFila3.setText(""+negocio.resultados(3));
		
		
		ResultadoCol0 = new JTextField();
		ResultadoCol0.setText("0");
		ResultadoCol0.setFont(new Font("Tahoma", Font.PLAIN, 19));
		ResultadoCol0.setEditable(false);
		ResultadoCol0.setColumns(10);
		ResultadoCol0.setBounds(49, 212, 33, 31);
		frmTablero.getContentPane().add(ResultadoCol0);
		ResultadoCol0.setText(""+negocio.resultados(4));
		
		
		ResultadoCol1 = new JTextField();
		ResultadoCol1.setText("0");
		ResultadoCol1.setFont(new Font("Tahoma", Font.PLAIN, 19));
		ResultadoCol1.setEditable(false);
		ResultadoCol1.setColumns(10);
		ResultadoCol1.setBounds(108, 212, 33, 31);
		frmTablero.getContentPane().add(ResultadoCol1);
		ResultadoCol1.setText(""+negocio.resultados(5));
		
		ResultadoCol2 = new JTextField();
		ResultadoCol2.setText("0");
		ResultadoCol2.setFont(new Font("Tahoma", Font.PLAIN, 19));
		ResultadoCol2.setEditable(false);
		ResultadoCol2.setColumns(10);
		ResultadoCol2.setBounds(170, 212, 33, 31);
		frmTablero.getContentPane().add(ResultadoCol2);
		ResultadoCol2.setText(""+negocio.resultados(6));
		
		ResultadoCol3 = new JTextField();
		ResultadoCol3.setText("0");
		ResultadoCol3.setFont(new Font("Tahoma", Font.PLAIN, 19));
		ResultadoCol3.setEditable(false);
		ResultadoCol3.setColumns(10);
		ResultadoCol3.setBounds(229, 212, 33, 31);
		frmTablero.getContentPane().add(ResultadoCol3);
		ResultadoCol3.setText(""+ negocio.resultados(7));
		
		JLabel titulo = new JLabel("TABLERO");
		titulo.setFont(new Font("Tahoma", Font.PLAIN, 17));
		titulo.setBounds(370, 58, 121, 21);
		frmTablero.getContentPane().add(titulo);
		
		JLabel lblNewLabel = new JLabel("La suma de los numeros\r\n de las filas y de las \r\ncolumnas deben coincidir\r\n");
		lblNewLabel.setBounds(28, 2, 403, 21);
		frmTablero.getContentPane().add(lblNewLabel);
		
		negocio.verificarIngresoDeNumero();
		
		
	}
}
