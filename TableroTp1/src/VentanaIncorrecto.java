import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class VentanaIncorrecto {

	 JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VentanaIncorrecto window = new VentanaIncorrecto();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public VentanaIncorrecto() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.RED);
		frame.getContentPane().setForeground(new Color(255, 0, 0));
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("INCORRECTO");
		lblNewLabel.setFont(new Font("Arial Black", Font.PLAIN, 35));
		lblNewLabel.setBounds(78, 28, 319, 83);
		frame.getContentPane().add(lblNewLabel);
		
		JButton btnNewButton = new JButton("REINTENTAR");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CodigoNegocio.reintentar();
				frame.setVisible(false);
			}
		});
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 19));
		btnNewButton.setBounds(115, 121, 201, 37);
		frame.getContentPane().add(btnNewButton);
		
		
		JButton btnVerResultados = new JButton("VER RESULTADO");
		btnVerResultados.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				VentanaResultados otro = new VentanaResultados();
				otro.frame.setVisible(true);
				otro.mostrar(Tabla.negocio);
			}
		});
	
		btnVerResultados.setFont(new Font("Tahoma", Font.PLAIN, 19));
		btnVerResultados.setBounds(115, 182, 201, 37);
		frame.getContentPane().add(btnVerResultados);
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	
	

}
